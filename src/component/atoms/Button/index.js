import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { colors, fonts } from '../../../utils';
import BtnIconSend from './BtnIconSend';
import IconOnly from './IconOnly';

export default function Button({type, title, onPress, icon, disable, height, width}) {
    if (type === 'btn-icon-send'){
        return <BtnIconSend disable={disable} onPress={onPress}/>
    }
    if (type === 'icon-only'){
        return <IconOnly icon={icon} onPress={onPress}/>
    }
    if (disable){
        return (
            <View style={styles.disablebg}>
                <Text style={styles.disableText}>{title}</Text>
            </View>
        )
    }
    return (
        <TouchableOpacity style={styles.container(type, height, width)} onPress={ onPress }>
            <Text style={styles.text(type)}>{title}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: (type, height, width) => ({
        backgroundColor: type==='secondary' ? colors.button.secondary.background: colors.button.primary.background,
        paddingVertical: 10,
        borderRadius: 100,
        height: height,
        width: width,
        borderWidth: type==='secondary' ? 1: 0,
        borderColor: colors.button.primary.background
    }),
    text: (type) => ({
        fontSize: 18,
        textAlign: 'center',
        color: type==='secondary' ? colors.button.secondary.text : colors.button.primary.text,
        fontFamily: fonts.primary[600]
    }),
    disablebg: {
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: colors.button.disable.background,
    },
    disableText: {
        fontSize: 18,
        textAlign: 'center',
        color: colors.button.disable.text,
        fontFamily: fonts.primary[600]
    }
})
