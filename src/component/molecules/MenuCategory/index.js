import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { colors, fonts } from '../../../utils'
import Icons from 'react-native-vector-icons/MaterialCommunityIcons'

export default function MenuCategory({category, onPress}) {
    const Icon = () => {
        if (category === 'Site Data'){
            return (
              <View style={{backgroundColor: '#0BCAD4', height: 46, width: 46, borderRadius: 46/2, marginBottom: 28, alignItems:'center', justifyContent:'center'}}>
                  <Icons name="radio-tower" color={colors.white} size={35}/>
              </View>
            )
        }
        if (category === 'Remote Rectifier (VPN)'){
            return (
                <View style={{backgroundColor: '#0BCAD4', height: 46, width: 46, borderRadius: 46/2, marginBottom: 28, alignItems:'center', justifyContent:'center'}}>
                    <Icons name="power-socket" color={colors.white} size={35}/>
                </View>
            )
        }
        if (category === 'Complain and Potential Spot'){
            return (
                <View style={{backgroundColor: '#0BCAD4', height: 46, width: 46, borderRadius: 46/2, marginBottom: 28, alignItems:'center', justifyContent:'center'}}>
                    <Icons name="signal-cellular-1" color={colors.white} size={35}/>
                </View>
            )
        }
        if (category === 'To-Do List'){
            return (
                <View style={{backgroundColor: '#0BCAD4', height: 46, width: 46, borderRadius: 46/2, marginBottom: 28, alignItems:'center', justifyContent:'center'}}>
                    <Icons name="format-list-checks" color={colors.white} size={35}/>
                </View>
            )
        }
        return (
            <View style={{backgroundColor: '#0BCAD4', height: 46, width: 46, borderRadius: 46/2, marginBottom: 28, alignItems:'center', justifyContent:'center'}}>
                <Icons name="radio-tower" color={colors.white} size={35}/>
            </View>
        )
    }
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon />
            <Text style={styles.category}>{category}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 12,
        backgroundColor: colors.cardLight,
        alignSelf: 'flex-start',
        borderRadius: 10,
        marginRight: 10,
        width: 110,
        height: 143
    },
    illustration: {
        marginBottom: 28
    },
    label: {
        fontSize: 12,
        fontFamily: fonts.primary[300],
        color: colors.text.primary
    },
    category: {
        fontSize: 12,
        fontFamily: fonts.primary[500],
        color: colors.text.primary
    }
})
