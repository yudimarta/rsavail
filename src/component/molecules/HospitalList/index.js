import React from 'react';
import { View, TouchableOpacity, Text, Image, Dimensions, StyleSheet } from 'react-native';
import Icons from 'react-native-vector-icons/MaterialCommunityIcons';
import { Gap, Link } from '../../../component';
import { colors, fonts } from '../../../utils';

export default function HospitalList(props) {
    const { OnPress, name, address, minutes, phone, available, queue, OnMap, OnDetail, OnCall, ...others } = props;
    return (
        <TouchableOpacity style={{
            width:  Dimensions.get('window').width, backgroundColor: '#FFFFFF',alignContent: 'center', justifyContent: 'center', padding: 24, borderBottomWidth: 1,
            borderBottomColor: colors.border }}
            onPress={OnPress}
            {...others}
        >

            <View style={{
                width:  Dimensions.get('window').width,
                flexDirection: 'row'
            }}>
                <View style={{width:  Dimensions.get('window').width*0.55, justifyContent: 'space-between'}}>
                    <Text style={styles.name}>
                        {name}
                    </Text>
                    <Gap height={8}/>
                    <Text style={styles.desc}>
                        {address}
                    </Text>
                    <Gap height={10}/>
                    <TouchableOpacity style={{flexDirection: 'row', backgroundColor: colors.primary, height: 40, width: 150, borderRadius: 10, alignItems: 'center', justifyContent: 'space-evenly'}} onPress={OnCall}>
                        <Icons name="phone-classic" size={23} color="#FFFFFF"/>
                        <Text style={styles.phone}>
                            {phone ? phone : 'Not Available'}
                        </Text>
                    </TouchableOpacity>
                    <Gap height={10}/>
                    <Text style={styles.desc}>
                        Diperbarui {minutes} menit yang lalu
                    </Text>
                </View>
                <View style={{width:  Dimensions.get('window').width*0.33, alignItems: 'center', marginRight: 24}}>
                    <Text style={styles.txt}>
                        Tersedia:
                    </Text>
                    <Gap height={18}/>
                    <Text style={styles.queue}>
                        {available}
                    </Text>
                    <Gap height={18}/>
                    <Text style={styles.desc}>
                        {queue} Antrian
                    </Text>
                    <Gap height={18}/>
                    <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                        <View style={{height: 40, width: 70, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: colors.primary}}>
                            <Link title="Lokasi" size={15} onPress={OnMap}/>
                        </View>
                        <Gap width={8}/>
                        <View style={{height: 40, width: 70, alignItems: 'center', justifyContent: 'center', borderRadius: 10, backgroundColor: colors.primary}}>
                            <Link title="Detail" size={15} onPress={OnDetail}/>
                        </View>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    name: {
        fontSize: 18,
        fontFamily: fonts.primary[800],
        color: colors.text.primary
    },
    desc: {
        fontSize: 14,
        fontFamily: fonts.primary[300],
        color: colors.text.secondary
        // textTransform: 'capitalize'
    },
    phone: {
        fontSize: 15,
        fontFamily: fonts.primary[300],
        color: "#FFFFFF",
        textDecorationLine: 'underline'
        // textTransform: 'capitalize'
    },
    txt: {
        fontSize: 16,
        fontFamily: fonts.primary[300],
        color: colors.text.primary
        // textTransform: 'capitalize'
    },
    queue: {
        fontSize: 30,
        fontFamily: fonts.primary[900],
        color: colors.text.primary
        // textTransform: 'capitalize'
    }           
})
