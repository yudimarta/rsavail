import Splash from './SplashScreen';
import Home from './Home';
import Hospitals from './Hospitals';
import WebPage from './WebPage';
// import GetStarted from './GetStarted';
// import Login from './Login';
// import Register from './Register';
// import UploadPhoto from './UploadPhoto';
// import Home from './Home';

export { Splash, Home, Hospitals, WebPage };