import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Dimensions, TextInput, TouchableOpacity, ScrollView, BackHandler } from 'react-native';
import { colors, fonts } from '../../utils';
import { Button, Input, Link, Gap, Header, List } from '../../component';
import Icons from 'react-native-vector-icons/Ionicons';
import {Picker} from '@react-native-community/picker';
import axios from 'axios';
import { JSONProvince } from '../../assets';
import { useSelector, useDispatch } from 'react-redux';

export default function Home({navigation, route}) {
    const [listProvince, setListProvince] = useState([]);
    const [refreshing, setRefreshing] = useState(false);
    const itemCategory = route.params;
    const stateGlobal = useSelector(state => state);
    const [word, setWord] = useState('');
    const dispatch = useDispatch();

    useEffect(() => {
        setListProvince(JSONProvince.data)
    }, []);

    useEffect(() => {
        if(!word){
            setProvince()
        }
            return () => {
              console.log('-- Sitelist components cleanup and unmounted');
            };
    }, [word])

    const setProvince = () => {
        setListProvince(JSONProvince.data);

    }

    const filterByValue = (value) => {
        return JSONProvince.data.filter((data) =>  JSON.stringify(data).toLowerCase().indexOf(value.toLowerCase()) !== -1)
    }
 
    return (
        <View style={styles.page}>
            <Header type="dark" title="Pilih Provinsi" onPress={() => BackHandler.exitApp() }/>
            <View style={styles.searchBar}>
                <View style={styles.searchBarInside}>
                    {word ? 
                    <TouchableOpacity onPress={() => {
                            setWord('')
                            setListProvince(filterByValue(''))
                        }}>
                        <Icons name="close-outline" size={25} color="#979797" style={{ marginLeft: 13, marginTop: 17 }}/> 
                    </TouchableOpacity> :
                    <Icons name="search" size={20} color="#979797" style={{ marginLeft: 13, marginTop: 17 }}/>
                    }
                    <TextInput
                        placeholder="Cari Provinsi"
                        placeholderTextColor="#8E8E93"
                        style={styles.searchBarTextInput}
                        value={word}
                        autoFocus={true}
                        onChangeText={(val) => {
                            setWord(val)
                            setListProvince(filterByValue(val))
                        }}
                        onSubmitEditing={()=> {
                            setListProvince(filterByValue(word))
                        }}
                    />
                </View>
            </View>
            <ScrollView>
            {listProvince ? 
            listProvince.map(item => {
                if(item){
                    return (
                        <List 
                          type="next"
                          key={item.id}
                          name={item.name}
                          onPress={() => navigation.navigate('Hospitals', item)}
                        />
                        
                    ) 
                    
                }
            })
            :
            <View>
                <Text>Not Found</Text>
            </View>
            }
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    page: {
        flex: 1,
        backgroundColor: colors.white
    },
    searchBar: {
        height: Dimensions.get('window').height * 0.1,
        backgroundColor: '#FFFFFF',
        paddingHorizontal: 24,
        justifyContent: 'center',
        marginTop: 12
      },
      searchBarInside: {
        flexDirection: 'row',
        backgroundColor: '#F3F1F1',
        borderRadius: 8,
        width: '100%'
      },
      searchBarTextInput: {
        height: 56,
        width: 320,
        fontSize: 14,
        lineHeight: 21,
        paddingLeft: 13, fontFamily: fonts.primary[300]
      },
      result: {
          fontSize: 14,
          paddingLeft: 13,
          fontFamily: fonts.primary[300],
          marginBottom: 12
      }
})
