import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { ILLogo } from '../../assets';
// import auth from '@react-native-firebase/auth';
// import database from '@react-native-firebase/database';
import { colors, fonts } from '../../utils';
// import { Firebase } from '../../config';

const Splash = ({navigation}) => {
  useEffect(() => {
    // const unsubscribe = auth().onAuthStateChanged((user) => {
      setTimeout(() => {
    //     if(user){
    //     //   console.log('user: ', user);
    //     //   navigation.replace('MainApp');
    //       // navigation.replace('GetStarted');
    //     } else {
          navigation.replace('Home');
    //     }
      }, 3000)
    // })
    // return () => unsubscribe();
  }, [navigation])
  
  return (
    <View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
      <View style={styles.page}>
        <Image source={ILLogo} resizeMode='contain' style={{maxHeight: 300}} />
      </View>
      <View style={{alignItems: 'center', marginBottom: 30}}>
        <Text>
          API by @agallio
        </Text>
        <Text>
          Front End by @yudimarta.id and @bagaskaranugroho
        </Text>
      </View>
    </View>
  )
}
  
  export default Splash;

const styles = StyleSheet.create({
    page: {
        backgroundColor: colors.white,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    title: {
        fontSize: 20,
        fontFamily: fonts.primary[600],
        color: colors.text.primary,
        marginTop: 20
    }
})
