import { createStore } from 'redux'

const initialState = {
    loading: false,
    name: 'Yudi Marta',
    address: 'Lamongan',
    weather: [],
    site: []
}

const reducer = (state = initialState, action) => {
    if(action.type === 'SET_LOADING'){
        return {
            ...state,
            loading: action.value
        }
    }
    // if(action.type === 'SET_NAME'){
    //     return {
    //         ...state,
    //         name: 'Azka'
    //     }
    // }
    if(action.type === 'SET_WEATHER'){
        return {
            ...state,
            weather: action.value
        }
    }
    if(action.type === 'SET_SITE'){
        return {
            ...state,
            site: action.value
        }
    }
    return state;
}

const store = createStore(reducer)

export default store;
